import { defineConfig } from "npm:vite@^5.0.12"
import vue from "npm:@vitejs/plugin-vue@^5.0.3"
import svg from "npm:vite-svg-loader@^5.1.0"

import "npm:vue@^3.4.15"

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue(), svg()],
  publicDir: "copiedAssets",
  build: {
    outDir: "public",
  },
})
